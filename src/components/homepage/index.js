import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Dimensions,
} from 'react-native';
import { isEmpty, equals } from 'ramda'
import { SearchBar, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

// Teletubbies https://www.youtube.com/watch?v=9gfqznk0CmU
// Ba ba black sheep https://www.youtube.com/watch?v=i7ygKQunfmE

const dim = Dimensions.get('window')
class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isReadyA: false,
            statusA: '',
            errorA: '',
            qualityA: '',
            isReadyB: false,
            statusB: '',
            errorB: '',
            qualityB: '',
            ready: false,
            list: {},
            keyword: 'songs for kids'
        }
    }

    static navigatorStyle = {
        navBarHidden: true
    };

    componentDidMount() {
        this.callYtApi()
    }

    callYtApi() {
        fetch(`https://www.googleapis.com/youtube/v3/search?part=snippet&q=${this.state.keyword}&type=video&key=AIzaSyCjg7CVf6P7JlJ_I5Y584u0Lt42nOskB1c`)
            .then(result => {
                const res = JSON.parse(result._bodyText)
                console.log(JSON.parse(result._bodyText))
                this.setState({
                    list: res,
                    ready: true,
                })
            })
    }

    render() {
        console.log(this.state)
        return (
            <View style={styles.container}>
                <View style={styles.divA}>
                    {/* <View style={styles.float}>
                            <Image
                                source={require('../../../res/img/sun.png')}
                                style={styles.flx1}
                                width={100}
                                height={100} />
                            
                        </View> */}
                    <SearchBar
                        lightTheme
                        showLoading
                        round
                        platform="android"
                        cancelIcon={{ type: 'font-awesome', name: 'chevron-left' }}
                        placeholder='Search'
                        onChangeText={(text) => {
                            this.setState({ keyword: text })
                            console.log(text)
                        }}
                        containerStyle={{ flex: 1 }}
                        onClear={(e) => {
                            console.log('OnClear is called with: ')
                            console.log(e)
                        }} />

                    {/* <Button
                        icon={
                            <Icon
                                name='arrow-right'
                                size={15}
                                color='white'
                            />
                        }
                        iconRight
                        title='BUTTON WITH RIGHT ICON'
                    /> */}
                    <Button
                        title=""
                        icon={
                            <Icon
                                name='search'
                                size={10}
                                color='white'
                            />
                        }
                        
                        onPress={() => {
                            if (!equals(this.state.keyword, '')) {
                                this.callYtApi()
                            }
                        }
                        }
                        
                        titleStyle={{ fontSize: 5 }}
                        buttonStyle={{
                            backgroundColor: "rgba(92, 99,216, 1)",
                            marginTop: 5,
                            width: 45,
                            height: 45,
                            borderColor: "transparent",
                            borderWidth: 0,
                            borderRadius: 5
                        }}
                        containerStyle={{ marginTop: 20 }}
                    />

                </View>
                {this.state.ready ?
                    <View style={styles.divB}>
                        <ScrollView
                            style={styles.scroll}
                            contentContainerStyle={{
                                justifyContent: 'center'
                            }}
                        >
                            {
                                this.state.list.items.map(vid => (
                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate('video', { videoData: vid })}
                                    >
                                        <View style={styles.vidContainer}>
                                            <Image
                                                source={{ uri: vid.snippet.thumbnails.high.url }}
                                                style={styles.flx1}
                                                width={dim.width}
                                                height={300}
                                            />
                                            <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 10 }}>
                                                <Text style={styles.textTitle}>
                                                    {vid.snippet.title}
                                                </Text>
                                                <Text style={styles.textSubTitle}>
                                                    {vid.snippet.channelTitle}
                                                </Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                ))
                            }
                        </ScrollView>
                    </View> : <View style={styles.divB}><ActivityIndicator size="large" color="#0000ff" /></View>}
            </View>
        )

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        width: '100%'
    },
    divA: {
        backgroundColor: '#FFF',
        alignItems: 'flex-start',
        flexDirection: 'row'
    },
    divB: {
        flex: 8,
        backgroundColor: '#000'
    },
    divC: {
        flex: 2,
        backgroundColor: '#000022'
    },
    flx1: {
        flex: 1,
        width: 100,
        height: 100
    },
    textTitle: {
        color: '#F00',
        fontSize: 20,
        fontWeight: '900'
    },
    textSubTitle: {
        color: '#F00',
        fontSize: 14,
        fontWeight: '700'
    },
    scroll: {
        flex: 1,
        backgroundColor: '#FFF'

    },
    float: {
        position: 'absolute',

    },
    vidContainer: {
        margin: 10,
        flex: 0,
        width: dim.width,
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center'

    }
})

export default HomePage;