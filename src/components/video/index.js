import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'
import YouTube from 'react-native-youtube'

export default class Videos extends React.Component {



    render() {
        const { videoData } = this.props.navigation.state.params
        return (
            <View>
                <YouTube
                    videoId={videoData.id.videoId}   // The YouTube video ID
                    play={true}             // control playback of video with true/false
                    fullscreen={true}       // control whether the video should play in fullscreen or inline
                    loop={true}             // control whether the video should loop when ended
                    apiKey='AIzaSyCjg7CVf6P7JlJ_I5Y584u0Lt42nOskB1c'
                    onReady={e => this.setState({ isReady: true })}
                    onChangeState={e => this.setState({ status: e.state })}
                    onChangeQuality={e => this.setState({ quality: e.quality })}
                    onError={e => this.setState({ error: e.error })}

                    style={{ alignSelf: 'stretch', height: 300 }}
                />
                <View style={{ flexDirection: 'column', justifyContent: 'space-between', marginLeft: 10 }}>
                    <Text style={styles.textTitle}>
                        {videoData.snippet.title}
                    </Text>
                    <Text style={styles.textSubTitle}>
                        {videoData.snippet.channelTitle}
                    </Text>
                </View>
                <View style={styles.desc}>
                    <Text>
                        {videoData.snippet.description}
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textTitle: {
        color: '#000',
        fontSize: 20,
        fontWeight: '900'
    },
    textSubTitle: {
        color: '#F00',
        fontSize: 14,
        fontWeight: '700'
    },
    desc: {
        marginLeft: 50,
        marginRight: 50,
        marginTop: 100,
        
    }
})